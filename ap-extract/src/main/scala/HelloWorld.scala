import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by oliviervs on 14/09/15.
 */
object HelloWorld {
  def main(args: Array[String]): Unit ={
    println("Hello World")
    val conf = new SparkConf()
    .setAppName("HelloWorld")
    .setMaster("local")

    val sc = new SparkContext(conf)

    val text = sc.textFile("words.txt")
    text.flatMap(line => line.split(" ").map(word => (word , 1))).reduceByKey(_ + _).foreach(println)
  }
}
